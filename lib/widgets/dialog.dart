import 'package:flutter/material.dart';

Future<void> showAlertDialog(context,
    {required String title,
    required String desc,
    required VoidCallback btnOK,
    required bool twoBtn,
    required String okStr}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: Text(desc),
        actions: <Widget>[
          twoBtn
              ? TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              : const SizedBox(),
          TextButton(onPressed: btnOK, child: Text(okStr)),
        ],
      );
    },
  );
}
