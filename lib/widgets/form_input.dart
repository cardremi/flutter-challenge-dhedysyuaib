import 'package:flutter/material.dart';

Widget formInput(
    {required String title,
    required String hint,
    required controller,
    required TextInputType inputType,
    bool secureText = false,
    bool enabled = true,
    required onChanged,
    required validator,
    required bool iconBtn,
    required VoidCallback onShowPassword}) {
  return TextFormField(
    controller: controller,
    onChanged: onChanged,
    keyboardType: inputType,
    obscureText: secureText,
    enabled: enabled,
    style: const TextStyle(fontSize: 13, color: Colors.black),
    decoration: InputDecoration(
      labelText: title,
      floatingLabelBehavior: FloatingLabelBehavior.always,
      labelStyle: const TextStyle(
          color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
      hintText: hint,
      hintStyle: const TextStyle(
          fontStyle: FontStyle.italic, color: Colors.grey, fontSize: 13),
      suffixIcon: iconBtn
          ? IconButton(
              padding: EdgeInsets.only(top: 20),
              iconSize: 18,
              color: Colors.grey,
              onPressed: onShowPassword,
              icon: secureText
                  ? Icon(Icons.visibility_off)
                  : Icon(Icons.visibility))
          : null,
    ),
    autovalidateMode: AutovalidateMode.onUserInteraction,
    validator: validator,
  );
}
