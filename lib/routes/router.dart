import 'package:flutter/material.dart';
import 'package:infosystest/constants/routing_constants.dart';
import 'package:infosystest/views/auth/login.dart';
import 'package:infosystest/views/auth/register.dart';
import 'package:infosystest/views/home/home.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case homeViewRoute:
      return MaterialPageRoute(builder: (context) => HomeView());
    case loginViewRoute:
      return MaterialPageRoute(builder: (context) => LoginPage());
    case registerViewRoute:
      return MaterialPageRoute(builder: (context) => RegisterPage());
    default:
      return MaterialPageRoute(builder: (context) => LoginPage());
  }
}
