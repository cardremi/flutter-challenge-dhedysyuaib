import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:infosystest/constants/routing_constants.dart';
import 'package:infosystest/widgets/dialog.dart';
import 'package:infosystest/widgets/form_input.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _secureText = true;
  void _signInWithEmailAndPassword() async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: _emailController.text, password: _passwordController.text);

      // ignore: use_build_context_synchronously
      showAlertDialog(context, title: 'Sukses', desc: 'Login Berhasil',
          btnOK: () {
        Navigator.of(context).pushReplacementNamed(homeViewRoute);
      }, twoBtn: true, okStr: 'Home');
    } catch (e) {
      showAlertDialog(context, title: 'Gagal', desc: e.toString(), btnOK: () {
        Navigator.of(context).pop();
      }, twoBtn: false, okStr: 'Ok');
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: SizedBox(
            height: height,
            child: Column(
              children: [header(context), formBody(context), footer(context)],
            ),
          ),
        ),
      ),
    );
  }

  Widget header(context) {
    return Container(
      height: MediaQuery.of(context).size.height / 4.5,
      alignment: Alignment.topCenter,
      child: Row(
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 2.6,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("assets/images/header-login.png")),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 3.5,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.contain,
                  image: AssetImage("assets/images/logo.png")),
            ),
          ),
        ],
      ),
    );
  }

  Widget formBody(context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(30, 50, 30, 30),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: const Text(
                'Login',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 21),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 5, bottom: 15),
              alignment: Alignment.centerLeft,
              child: const Text(
                'Please sign in to continue',
                style: TextStyle(color: Colors.black, fontSize: 14),
              ),
            ),
            formInput(
                title: 'User ID',
                hint: 'User  ID',
                controller: _emailController,
                inputType: TextInputType.emailAddress,
                onChanged: null,
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your email';
                  }
                  return null;
                },
                onShowPassword: () {},
                iconBtn: false),
            const SizedBox(
              height: 20,
            ),
            formInput(
                title: 'Password',
                hint: 'Password',
                controller: _passwordController,
                inputType: TextInputType.visiblePassword,
                onChanged: null,
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some password';
                  }
                  return null;
                },
                secureText: _secureText,
                onShowPassword: () {
                  setState(() {
                    _secureText = !_secureText;
                  });
                },
                iconBtn: true),
            const SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.purple.shade800,
                    fixedSize: const Size(150, 50),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50))),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    _signInWithEmailAndPassword();
                  }
                },
                child: const Text(
                  'LOGIN',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget footer(context) {
    return Expanded(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text(
              "Don't have an account? ",
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 14),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(registerViewRoute);
              },
              child: const Text(
                'Sign Up',
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.w600,
                    fontSize: 13.8),
              ),
            )
          ]),
    );
  }
}
